### SWGSource public repo
by Erusman

Note: This repository is still a work in progress, as is this README. If you run into any issues while setting up your server or environment, please let us know on our website http://www.swgsource.com

This repository houses the source code to the actual game server and other various server applications for Star Wars Galaxies NGE.  It also contains source code to the game files (such as the client and server game data).

### Environment Setup Guide

The source code can be compiled using our custom VM. Download it here: https://www.swgsource.com/forums/threads/swg-nge-server-vm-debian-64-prebuilt-by-erusman.87/